function createSystemPic(elementId,log) {
    //console.log(log);
    Highcharts.chart(elementId, {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: '各系统使用率'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: log
        }]
    });
}

function normalLine(elementId,dataList,picX) {
    var chart = Highcharts.chart(elementId, {
        chart: {
            type: 'line'
        },
        title: {
            text: '系统使用折线图'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        subtitle: {

        },
        xAxis: {
            categories: picX
        },
        yAxis: {
            title: {
                text:  " 处理数"
            }
        },
        plotOptions: {
            series: {
                label: {
                    connectorAllowed: false
                },
                pointStart: 2010
            }
        },
        series: dataList
    });


}

function makeActionLine(elementId1,elementId2) {
    Highcharts.setOptions({
        global: {
            useUTC: false
        }
    });
    function activeLastPointToolip(chart,series) {
        var seriesAll=chart.series;
        for(var a=0;a<seriesAll.length;a++){
            var points=seriesAll[a].points;
            chart.tooltip.refresh(points[points.length -1]);
        }

    }
    var option = Highcharts.chart(elementId2, {
        chart: {
            type: 'spline',
            marginRight: 10,
            events: {
                load: function () {
                    var seriesAll = this.series,
                        chartAll = this;
                    activeLastPointToolip(chartAll,seriesAll[0]);
                    var oldTime=getNowTime();
                    setInterval(function () {
                        var dataList=[];
                        var size="9999";
                        var end=getNowTime();
                        var params = {
                            start: oldTime,
                            end: end,
                            size: size
                        };
                        $.ajax({
                            url: baseURL+'elasticsearch/getEncode',
                            type: 'POST',
                            data: params,
                            success: function (returnData) {
                                oldTime=end;
                                for(var key in returnData){
                                    dataList.push({
                                        name:key,
                                        y:returnData[key]
                                    });
                                    createSystemPic( elementId1,dataList);
                                        var x = (new Date()).getTime(), // 当前时间
                                            y = returnData[key];          // 随机值
                                    var series=getSeries(seriesAll,key),
                                        chart = chartAll;
                                        series.addPoint([x, y], true, true);
                                }
                                checkData(seriesAll,returnData,x);
                                activeLastPointToolip(chart, series);
                            },
                            error: function () {
                                console.log("获取失败");
                            }
                        });
                        }, 2000);

                }
            }
        },
        title: {
            text: '系统调用折线图'
        },
        xAxis: {
            type: 'datetime',
            tickPixelInterval: 150
        },
        yAxis: {
            title: {
                text: null
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle'
        },
        plotOptions: {
            line: {
                dataLabels: {
                    // 开启数据标签
                    enabled: true
                },
                // 关闭鼠标跟踪，对应的提示框、点击事件会失效
                enableMouseTracking: false
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    Highcharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/>' +
                    Highcharts.numberFormat(this.y, 2);
            }
        },
        series: [{
            name: '测试数据',
            data: (function () {
                // 生成随机值
                var data = [],
                    time = (new Date()).getTime(),
                    i;
                for (i = -19; i <= 0; i += 1) {
                    data.push({
                        x: time + i * 2000,
                        y: 0
                    });
                }
                return data;
            }())
        }]
    });

    function checkData(seriesAll,returnData,time) {
        for(var a=0;a<seriesAll.length;a++){
            if(!returnData.hasOwnProperty(seriesAll[a].name)){
                seriesAll[a].addPoint([time, 0], true, true);
            }
        }
    }

    function getSeries(seriesAll,name) {
        var find=false;
        for(var a=0;a<seriesAll.length;a++){
            if(seriesAll[a].name===name){
                find=true;
                return seriesAll[a];
            }
        }
        if(!find){
            option.addSeries({
                name: name,
                data: (function () {
                    // 生成随机值
                    var data = [],
                        time = (new Date()).getTime(),
                        i;
                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 2000,
                            y: 0
                        });
                    }
                    return data;
                }())
            });
            return getSeries(seriesAll,name);
        }
    }

    function getNowTime() {
        var date = new Date();
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        var minutes=date.getMinutes();
        var second=date.getSeconds();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        if (second >= 0 && second <= 9) {
            second = "0" + second;
        }
        if (minutes >= 0 && minutes <= 9) {
            minutes = "0" + minutes;
        }
        var currentDate = date.getFullYear() + "-" + month + "-" + strDate
            + " " + date.getHours() + ":" + minutes + ":" + second;
        return currentDate;
    }

}