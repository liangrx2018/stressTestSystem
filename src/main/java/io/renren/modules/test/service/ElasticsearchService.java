package io.renren.modules.test.service;

import java.util.Map;

public interface ElasticsearchService {
    public Map<String,Integer> getElasticsearchByTime(String start, String end, String size);
}
