package io.renren.modules.test.controller;


import io.renren.modules.test.service.ElasticsearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/elasticsearch")
public class ElasticsearchController {

    @Autowired
    private ElasticsearchService elasticsearchService;

    @RequestMapping(value = "/getEncode",method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Integer> getElasticsearchLogByTime(HttpServletRequest request){
        Map<String, String[]> parameterMap = request.getParameterMap();
        String start=parameterMap.get("start")[0];
        String end=parameterMap.get("end")[0];
        String size=parameterMap.get("size")[0];
        return elasticsearchService.getElasticsearchByTime(start,end,size);

    }

}
