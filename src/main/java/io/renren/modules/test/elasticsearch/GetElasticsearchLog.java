package io.renren.modules.test.elasticsearch;


import io.renren.modules.test.utils.HttpClientUtil;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class GetElasticsearchLog {

        public TransportClient getEcCon(){
            Settings settings = Settings.builder()
                    .put("cluster.name", "elasticsearch").build();
            try {
                TransportClient client = new PreBuiltTransportClient(settings)
                        .addTransportAddress(new TransportAddress(InetAddress.getByName("elasticsearch-client.biz.com"), 80));
                return client;
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            return null;
        }

        public void search(){
            TransportClient transportClient=getEcCon();

            SearchRequest searchRequest = new SearchRequest("logstash-test-2019.08.26");
            searchRequest.types("doc");

            TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("input_type", "log");;
            BoolQueryBuilder boolBuilderTemp = QueryBuilders.boolQuery();
            boolBuilderTemp.must(termQueryBuilder);
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            sourceBuilder.query(boolBuilderTemp);
            searchRequest.source(sourceBuilder);
            try {
                SearchResponse response = transportClient.search(searchRequest).get();
                System.out.println(response);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

    public static void main(String[] args) {
        GetElasticsearchLog getElasticsearchLog=new GetElasticsearchLog();
        getElasticsearchLog.search();
    }

}
